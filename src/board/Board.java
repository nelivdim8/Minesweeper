package board;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import random.RandomGenerator;

public class Board {
    private Cell[][] cells;
    private int minesCount;
    private RandomGenerator random;

    public Board(Level level) {
        this.cells = new Cell[level.getHeight()][level.getWidth()];
        this.minesCount = level.getMinesCount();
        this.random = new RandomGenerator();
    }

    public Cell getCell(int width, int height) {
        if (width < 0 || width > cells[0].length || height < 0 || height > cells.length) {
            return null;
        }
        return cells[height][width];
    }

    private void clearBoard() {
        for (Cell[] cells2 : cells) {
            Arrays.fill(cells2, null);
        }
    }

    private boolean shouldClear() {
        return cells[0][0] != null;
    }

    public void fillBoard() {
        if (shouldClear()) {
            clearBoard();
        }
        putMinesOnBoard();
        for (int i = 0; i < cells.length; i++) {
            for (int j = 0; j < cells[i].length; j++) {
                if (cells[i][j] == null) {
                    fillCell(i, j);
                }
            }
        }
    }

    private void fillCell(int i, int j) {
        List<Cell> neighbours = getCellNeighbourMines(j, i);
        if (neighbours.size() > 0) {
            cells[i][j] = new Cell(Type.getValueAndType().get(neighbours.size()), j, i);
        } else {
            cells[i][j] = new Cell(Type.EMPTY, j, i);

        }
    }

    private void putMinesOnBoard() {
        int randWidth;
        int randHeight;
        int temp = minesCount;
        for (int i = minesCount; i > 0; i--) {
            do {

                randWidth = random.generateNumber(temp++, 0, cells[0].length);
                randHeight = random.generateNumber(i, 0, cells.length);
            } while (cells[randHeight][randWidth] != null);
            cells[randHeight][randWidth] = new Cell(Type.MINE, randWidth, randHeight);
        }
    }

    private List<Cell> getCellNeighbourMines(int width, int height) {
        List<Cell> neighbours = new LinkedList<>();
        addNeigbourMine(width, height + 1, neighbours);
        addNeigbourMine(width + 1, height, neighbours);
        addNeigbourMine(width, height - 1, neighbours);
        addNeigbourMine(width - 1, height, neighbours);
        addNeigbourMine(width + 1, height + 1, neighbours);
        addNeigbourMine(width - 1, height - 1, neighbours);
        addNeigbourMine(width - 1, height + 1, neighbours);
        addNeigbourMine(width + 1, height - 1, neighbours);
        return neighbours;

    }

    public int getNeighbourMinesCount(int width, int height) {
        return getCellNeighbourMines(width, height).size();
    }

    public void openCell(int width, int height) {
        cells[height][width].open();
    }

    public Set<Cell> openEmptyCells(int width, int height) {
        Set<Cell> openedCells = new HashSet<>();
        openEmptyCellsHelper(width, height, openedCells);
        return openedCells;
    }

    private void openEmptyCellsHelper(int width, int height, Set<Cell> openedCells) {
        if (!areValidIndexes(width, height)) {
            return;
        }
        if (cells[height][width].getState() == State.OPEN) {
            return;
        }
        cells[height][width].open();
        openedCells.add(cells[height][width]);
        if (cells[height][width].getType() == Type.EMPTY) {
            openEmptyCellsHelper(width, height + 1, openedCells);
            openEmptyCellsHelper(width + 1, height, openedCells);
            openEmptyCellsHelper(width, height - 1, openedCells);
            openEmptyCellsHelper(width - 1, height, openedCells);
            openEmptyCellsHelper(width + 1, height + 1, openedCells);
            openEmptyCellsHelper(width - 1, height - 1, openedCells);
            openEmptyCellsHelper(width - 1, height + 1, openedCells);
            openEmptyCellsHelper(width + 1, height - 1, openedCells);
        }
    }

    private boolean addNeigbourMine(int width, int height, List<Cell> neighbours) {
        if (areValidIndexes(width, height) && cells[height][width] != null
                && cells[height][width].getType() == Type.MINE) {
            neighbours.add(cells[height][width]);
            return true;
        }
        return false;
    }

    private boolean areValidIndexes(int width, int height) {
        return (width < cells[0].length && width >= 0) && (height < cells.length && height >= 0);

    }

    public void openMines() {
        for (int i = 0; i < cells.length; i++) {
            for (int j = 0; j < cells[i].length; j++) {
                if (cells[i][j].getType() == Type.MINE) {
                    cells[i][j].open();
                }
            }
        }
    }

    public int getCellsNumber() {
        return cells.length * cells[0].length;
    }

    public int getAllCellsToOpen() {
        return cells.length * cells[0].length - minesCount;
    }

    public State mark(Cell cell) {
        return cell.mark();
    }

    public Cell[][] getCells() {
        Cell[][] copy = new Cell[cells.length][cells[0].length];
        for (int i = 0; i < cells.length; i++) {
            for (int j = 0; j < cells[i].length; j++) {
                if (cells[i][j] != null) {
                    copy[i][j] = new Cell(cells[i][j]);
                }
            }
        }
        return copy;
    }

    public Cell verifyCellIsNotMine(int width, int height) {
        Cell cell = getCell(width, height);
        while (cell.getType() == Type.MINE) {
            fillBoard();
            cell = getCell(width, height);
        }
        return cell;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < cells.length; i++) {
            for (int j = 0; j < cells[i].length; j++) {
                sb.append(cells[i][j].getSymbol());
                if (j < cells[i].length - 1) {
                    sb.append(" ");
                }
            }
            sb.append("\n");
        }
        return sb.toString();
    }

}
