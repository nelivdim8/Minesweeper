package board;

public class Cell {
    private State state;
    private Type type;
    private final int height;
    private final int width;

    public Cell(State state, Type type, int width, int height) {
        this.state = state;
        this.type = type;
        this.width = width;
        this.height = height;
    }

    public Cell(Type type, int width, int height) {
        this.state = State.CLOSED;
        this.type = type;
        this.width = width;
        this.height = height;
    }

    public Cell(Cell cell) {
        this.state = cell.state;
        this.type = cell.type;
        this.width = cell.width;
        this.height = cell.height;
    }

    public char getSymbol() {
        return state.getSymbol(type);
    }

    public void open() {
        if (this.state == State.CLOSED) {
            this.state = State.OPEN;
        }
    }

    public State mark() {
        if (state == State.CLOSED) {
            return state = State.FLAG;
        }
        if (state == State.FLAG) {
            return state = State.QUESTION;
        }
        if (state == State.QUESTION) {
            return state = State.CLOSED;
        }

        return state;

    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public State getState() {
        return state;
    }

    public Type getType() {
        return type;
    }

    @Override
    public String toString() {
        return getSymbol() + " ";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + height;
        result = prime * result + ((state == null) ? 0 : state.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        result = prime * result + width;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Cell other = (Cell) obj;
        if (height != other.height)
            return false;
        if (state != other.state)
            return false;
        if (type != other.type)
            return false;
        if (width != other.width)
            return false;
        return true;
    }

}
