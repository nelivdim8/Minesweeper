package board;

public class Choice {
    private Cell cell;
    private int option;

    public Choice(Cell cell, int option) {
        this.cell = cell;
        this.option = option;
    }

    public Cell getCell() {
        return cell;
    }

    public Type getCellType() {
        return cell.getType();
    }

    public int getWidth() {
        return cell.getWidth();
    }

    public int getHeight() {
        return cell.getHeight();
    }

    public int getOption() {
        return option;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((cell == null) ? 0 : cell.hashCode());
        result = prime * result + option;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Choice other = (Choice) obj;
        if (cell == null) {
            if (other.cell != null)
                return false;
        } else if (!cell.equals(other.cell))
            return false;
        if (option != other.option)
            return false;
        return true;
    }

}
