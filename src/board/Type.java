package board;

import java.util.HashMap;
import java.util.Map;

public enum Type {
    MINE(-1, '*'), EMPTY(0, ' '), ONE(1, '1'), TWO(2, '2'), THREE(3, '3'), FOUR(4, '4'), FIVE(5, '5'), SIX(6,
            '6'), SEVEN(7, '7'), EIGHT(8, '8');

    private final int value;
    private final char symbol;
    private static Map<Integer, Type> valueAndType = new HashMap<>();

    static {
        for (Type type : Type.values()) {
            valueAndType.put(type.value, type);
        }
    }

    private Type(int value, char open) {
        this.value = value;
        this.symbol = open;

    }

    public static Map<Integer, Type> getValueAndType() {
        return valueAndType;
    }

    public char getSymbol() {
        return symbol;
    }

    public int getValue() {
        return value;
    }

}
