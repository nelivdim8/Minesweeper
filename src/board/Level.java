package board;

public enum Level {
    BEGINNER(8, 8, 10), INTERMEDIATE(16, 16, 40), EXPERT(31, 16, 99);
    private int width;
    private int height;
    private int minesCount;

    private Level(int width, int height, int minesCount) {
        this.width = width;
        this.height = height;
        this.minesCount = minesCount;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getMinesCount() {
        return minesCount;
    }

}
