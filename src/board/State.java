package board;

public enum State {
    OPEN(' '), FLAG('F'), QUESTION('?'), CLOSED('\u25A1');
    private char symbol;

    private State(char symbol) {
        this.symbol = symbol;
    }

    public char getSymbol(Type type) {
        return (this != State.OPEN) ? symbol : type.getSymbol();

    }

}
