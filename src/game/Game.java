package game;

import board.Board;
import board.Cell;
import board.Choice;
import board.Level;
import board.Type;
import input.Input;
import timer.GameTimer;

public class Game {
    private Board board;
    private Input input;
    private GameTimer timer;
    private Choice choice;
    private int minesLeftToFind;
    private int cellsOpened;

    public Game(Level level) {
        this.board = new Board(level);
        this.input = new Input();
        this.timer = new GameTimer();
        this.cellsOpened = 0;
        this.minesLeftToFind = level.getMinesCount();

    }

    public void start() {
        board.fillBoard();
        while (true) {
            printInfo();
            this.choice = getChoice();
            if (choice.getOption() == 1) {
                board.mark(choice.getCell());
            } else {
                if (!openCell(choice)) {
                    displayResult("You lose! \u2639");
                    break;
                }
                if (cellsOpened == board.getAllCellsToOpen()) {
                    displayResult("You win! \u263A");
                    break;
                }
            }
        }
    }

    public boolean openCell(Choice choice) {
        if (choice.getCellType() == Type.MINE) {
            board.openMines();
            return false;
        } else if (choice.getCellType() == Type.EMPTY) {
            board.openEmptyCells(choice.getWidth(), choice.getHeight());
        } else {
            board.openCell(choice.getWidth(), choice.getHeight());
        }
        cellsOpened++;

        return true;
    }

    public void displayResult(String result) {
        printInfo();
        System.out.println(result);
    }

    public Choice getChoice() {
        int height = input.getInput("Enter row: \n");
        int width = input.getInput("Enter column: \n");
        int option = chooseOption();
        Cell cell = chooseCell(option, width, height);
        return new Choice(cell, option);
    }

    private Cell chooseCell(int option, int width, int height) {
        return (cellsOpened == 0 && option == 2) ? board.verifyCellIsNotMine(width, height)
                : board.getCell(width, height);
    }

    private int chooseOption() {
        int option;
        do {
            option = input.getInput("Choose option: \n1.Mark cell\n2.Open cell");
        } while (option != 1 && option != 2);
        return option;
    }

    private void printInfo() {
        System.out.println(board.toString());
        System.out.println("Mines left: " + minesLeftToFind + "  " + timer.toString());
    }

}
