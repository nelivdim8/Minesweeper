package input;

import java.util.Scanner;

public class Input {
    private Scanner input;

    public Input() {
        this.input = new Scanner(System.in);
    }

    public int getInput(String message) {
        String text;
        do {
            System.out.println(message);
            text = input.next();
        } while (!text.matches("[0-9]+"));
        return Integer.parseInt(text);
    }

}
