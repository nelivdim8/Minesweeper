package random;

import java.util.Random;

public class RandomGenerator {
    private Random rand;

    public RandomGenerator(long seed) {
        this.rand = new Random(seed);
    }

    public RandomGenerator() {
        this.rand = new Random();
    }

    public int generateNumber(int from, int length) {
        return from + rand.nextInt(length);

    }

    public int generateNumber(int count, int from, int length) {
        return from + rand.nextInt(length);

    }

}
