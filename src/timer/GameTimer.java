package timer;

public class GameTimer {
    private int secondsElapsed;
    private final long startingTime;
    private final static int MAX_SECONDS = 999;

    public GameTimer() {
        this.secondsElapsed = 0;
        this.startingTime = System.currentTimeMillis();
    }

    public int getSecondsElapsed() {
        return secondsElapsed;
    }

    public void setSecondsElapsed() {
        int seconds = (int) (System.currentTimeMillis() - startingTime) / 1000;
        if (seconds > secondsElapsed || seconds == MAX_SECONDS) {
            this.secondsElapsed = seconds;
        }
    }

    @Override
    public String toString() {
        setSecondsElapsed();
        return "Seconds elapsed: " + secondsElapsed + "\n";
    }

}
