package board;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class BoardTest {

    private Board board;
    private Cell[][] cells;
    private Level level1 = Level.BEGINNER;
    private Level level2 = Level.INTERMEDIATE;
    private Level level3 = Level.EXPERT;

    public void setData(Level level) {
        board = new Board(level);
        board.fillBoard();
        cells = board.getCells();

    }

    public Cell getFirst(Type type) {
        Cell first = null;
        for (int i = 0; i < cells.length; i++) {
            for (int j = 0; j < cells[i].length; j++) {
                if (cells[i][j].getType() == type) {
                    first = cells[i][j];
                    break;
                }
            }
        }
        return first;
    }

    public void setBoard(Level level) {
        board = new Board(level);
        cells = board.getCells();

    }

    @Test
    public void When_FillBoard_Expect_NotToContainNullValues() {
        setData(level1);
        for (int i = 0; i < cells.length; i++) {
            for (int j = 0; j < cells[i].length; j++) {
                assertNotNull(cells[i][j]);
            }
        }
    }

    @Test
    public void When_FillBoard_Expect_ToContainTenMines() {
        setData(level1);
        int counter = 0;
        for (int i = 0; i < cells.length; i++) {
            for (int j = 0; j < cells[i].length; j++) {
                if (cells[i][j].getType() == Type.MINE) {
                    counter++;
                }
            }
        }
        assertEquals(10, counter);

    }

    @Test
    public void When_FillBoard_Expect_ToContainCorrectValues() {
        setData(level1);
        for (int i = 0; i < cells.length; i++) {
            for (int j = 0; j < cells[i].length; j++) {
                if (cells[i][j].getType() != Type.MINE) {
                    assertEquals(cells[i][j].getType().getValue(), board.getNeighbourMinesCount(j, i));
                }
            }
        }
    }

    @Test
    public void When_CellHasValidWidthAndHeight_Expect_ToReturnCurrentCell() {
        setData(level1);
        assertNotNull(board.getCell(0, 0));

    }

    @Test
    public void When_CellHasValidWidthAndHeight_Expect_ToReturnValidType() {
        setData(level1);
        assertTrue(board.getCell(0, 0) instanceof Cell);

    }

    @Test
    public void When_CellHasValidWidthAndHeightGreaterThanUpperBound_Expect_ToReturnNull() {
        setData(level1);
        assertNull(board.getCell(1, cells.length + 2));

    }

    @Test
    public void When_CellHasValidHeightAndWidthGreaterThanUpperBound_Expect_ToReturnNull() {
        setData(level1);
        assertNull(board.getCell(cells[0].length + 2, 1));

    }

    @Test
    public void When_CellHasHeightAndWidthGreaterThanUpperBound_Expect_ToReturnNull() {
        setData(level1);
        assertNull(board.getCell(cells[0].length + 2, cells.length + 2));

    }

    @Test
    public void When_CellHasHeightAndWidthLessThanLowerBound_Expect_ToReturnNull() {
        setData(level1);
        assertNull(board.getCell(-1, -1));

    }

    @Test
    public void When_CellHasValidHeightAndWidthLessThanLowerBound_Expect_ToReturnNull() {
        setData(level1);
        assertNull(board.getCell(-1, 2));

    }

    @Test
    public void When_CellHasValidWidthAndHeightLessThanLowerBound_Expect_ToReturnNull() {
        setData(level1);
        assertNull(board.getCell(2, -1));

    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void When_When_CellHasValidWidthAndHeightGreaterThanUpperBound_Expect_OpenCellToThrowAnException() {
        setData(level1);
        board.openCell(1, cells.length + 2);

    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void When_CellHasValidHeightAndWidthGreaterThanUpperBound_Expect_OpenCellToThrowAnException() {
        setData(level1);
        board.openCell(cells[0].length + 2, 1);

    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void When_CellHasHeightAndWidthGreaterThanUpperBound_Expect_OpenCellToThrowAnException() {
        setData(level1);
        board.openCell(cells[0].length + 2, cells.length + 2);

    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void When_CellHasHeightAndWidthLessThanLowerBound_Expect_OpenCellToThrowAnException() {
        setData(level1);
        board.openCell(-1, -1);

    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void When_CellHasValidHeightAndWidthLessThanLowerBound_Expect_OpenCellToThrowAnException() {
        setData(level1);
        board.openCell(-1, 2);

    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void When_CellHasValidWidthAndHeightLessThanLowerBound_Expect_OpenCellToThrowAnException() {
        setData(level1);
        board.openCell(2, -1);

    }

    @Test
    public void When_CellHasValidWidthAndHeight_Expect_CurrentCellStateToBeOpen() {
        setData(level1);
        board.openCell(2, 2);
        Cell cell = board.getCell(2, 2);
        assertTrue(cell.getState() == State.OPEN);

    }

    @Test
    public void When_OpenClosedMines_Expect_AllMinesStateToBeOpen() {
        setData(level1);
        board.openMines();
        cells = board.getCells();
        for (int i = 0; i < cells.length; i++) {
            for (int j = 0; j < cells[i].length; j++) {
                if (cells[i][j].getType() == Type.MINE) {
                    assertTrue(cells[i][j].getState() == State.OPEN);
                }

            }
        }
    }

    @Test
    public void When_OpenMinesWhichAreAlreadyOpen_Expect_AllMinesStateToBeOpen() {
        setData(level1);
        board.openMines();
        cells = board.getCells();
        for (int i = 0; i < cells.length; i++) {
            for (int j = 0; j < cells[i].length; j++) {
                if (cells[i][j].getType() == Type.MINE) {
                    assertTrue(cells[i][j].getState() == State.OPEN);
                }

            }
        }

    }

    @Test
    public void When_GetCellsNumberAtBeginningLevel_Expect_ToReturnCorrectNumber() {
        setData(level1);
        int expectedNumber = level1.getHeight() * level1.getWidth();
        int actual = board.getCellsNumber();
        assertEquals(expectedNumber, actual);

    }

    @Test
    public void When_GetCellsNumberAtIntermediateLevel_Expect_ToReturnCorrectNumber() {
        setData(level2);
        int expectedNumber = level2.getHeight() * level2.getWidth();
        int actual = board.getCellsNumber();
        assertEquals(expectedNumber, actual);

    }

    @Test
    public void When_GetCellsNumberAtExpertLevel_Expect_ToReturnCorrectNumber() {
        setData(level3);
        int expectedNumber = level3.getHeight() * level3.getWidth();
        int actual = board.getCellsNumber();
        assertEquals(expectedNumber, actual);

    }

    @Test
    public void When_GetAllCellsToOpenAtBeginningLevel_Expect_ToReturnCorrectNumber() {
        setData(level1);
        int expectedNumber = level1.getHeight() * level1.getWidth() - level1.getMinesCount();
        int actual = board.getAllCellsToOpen();
        assertEquals(expectedNumber, actual);

    }

    @Test
    public void When_GetAllCellsToOpenAtIntermediateLevel_Expect_ToReturnCorrectNumber() {
        setData(level2);
        int expectedNumber = level2.getHeight() * level2.getWidth() - level2.getMinesCount();
        int actual = board.getAllCellsToOpen();
        assertEquals(expectedNumber, actual);

    }

    @Test
    public void When_GetAllCellsToOpenAtExpertLevel_Expect_ToReturnCorrectNumber() {
        setData(level3);
        int expectedNumber = level3.getHeight() * level3.getWidth() - level3.getMinesCount();
        int actual = board.getAllCellsToOpen();
        assertEquals(expectedNumber, actual);

    }

    @Test
    public void When_BoardIsEmpty_Expect_ToContainOnlyNullValues() {
        setBoard(level1);
        for (int i = 0; i < cells.length; i++) {
            for (int j = 0; j < cells[i].length; j++) {
                assertNull(cells[i][j]);

            }
        }

    }

    @Test
    public void When_MarkClosedCell_Expect_ToReturnFlagState() {
        setBoard(level1);
        Cell cell = new Cell(State.CLOSED, Type.EIGHT, 0, 0);
        assertEquals(board.mark(cell), State.FLAG);

    }

    @Test
    public void When_MarkClosedCell_Expect_CellStateToBecameFlag() {
        setBoard(level1);
        Cell cell = new Cell(State.CLOSED, Type.EIGHT, 0, 0);
        board.mark(cell);
        assertEquals(cell.getState(), State.FLAG);

    }

    @Test
    public void When_MarkOpenCell_Expect_ToReturnOpenState() {
        setBoard(level1);
        Cell cell = new Cell(State.OPEN, Type.EIGHT, 0, 0);
        assertEquals(board.mark(cell), State.OPEN);

    }

    @Test
    public void When_MarkOpenCell_Expect_CellStateToRemainOpen() {
        setBoard(level1);
        Cell cell = new Cell(State.OPEN, Type.EIGHT, 0, 0);
        cell.mark();
        assertEquals(board.mark(cell), State.OPEN);

    }

    @Test
    public void When_MarkFlagCell_Expect_ToReturnQuestionState() {
        setBoard(level1);
        Cell cell = new Cell(State.FLAG, Type.EIGHT, 0, 0);
        assertEquals(board.mark(cell), State.QUESTION);

    }

    @Test
    public void When_MarkFlagCell_Expect_CellStateToBecameQuestion() {
        setBoard(level1);
        Cell cell = new Cell(State.FLAG, Type.EIGHT, 0, 0);
        board.mark(cell);
        assertEquals(cell.getState(), State.QUESTION);

    }

    @Test
    public void When_MarkQuestionCell_Expect_ToReturnClosedState() {
        setBoard(level1);
        Cell cell = new Cell(State.QUESTION, Type.EIGHT, 0, 0);
        assertEquals(board.mark(cell), State.CLOSED);

    }

    @Test
    public void When_MarkQuestionCell_Expect_CellStateToBecameClosed() {
        setBoard(level1);
        Cell cell = new Cell(State.QUESTION, Type.EIGHT, 0, 0);
        board.mark(cell);
        assertEquals(cell.getState(), State.CLOSED);

    }

    @Test
    public void When_CellIsMine_Expect_VerifyCellIsNotMineToReturnCellWhichTypeIsNotMine() {
        setData(level1);
        Cell cell = getFirst(Type.MINE);
        Cell result = board.verifyCellIsNotMine(cell.getWidth(), cell.getHeight());
        assertTrue(result.getType() != Type.MINE);

    }

    @Test
    public void When_CellIsNotMine_Expect_VerifyCellIsNotMineToReturnCellWhichTypeIsNotMine() {
        setData(level1);
        Cell cell = getFirst(Type.ONE);
        assertTrue(board.verifyCellIsNotMine(cell.getWidth(), cell.getHeight()).getType() != Type.MINE);

    }

}
