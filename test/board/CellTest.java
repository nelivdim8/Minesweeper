package board;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class CellTest {

    @Test
    public void When_StateIsOpenAndTypeIsMine_Expect_ToReturnCorrectSymbol() {
        Cell cell = new Cell(State.OPEN, Type.MINE, 0, 0);
        assertEquals(cell.getState().getSymbol(Type.MINE), cell.getSymbol());

    }

    @Test
    public void When_StateIsOpenAndTypeIsEmpty_Expect_ToReturnCorrectSymbol() {
        Cell cell = new Cell(State.OPEN, Type.EMPTY, 0, 0);
        assertEquals(cell.getState().getSymbol(Type.EMPTY), cell.getSymbol());

    }

    @Test
    public void When_StateIsOpenAndTypeIsOne_Expect_ToReturnCorrectSymbol() {
        Cell cell = new Cell(State.OPEN, Type.ONE, 0, 0);
        assertEquals(cell.getState().getSymbol(Type.ONE), cell.getSymbol());

    }

    @Test
    public void When_StateIsOpenAndTypeIsTwo_Expect_ToReturnCorrectSymbol() {
        Cell cell = new Cell(State.OPEN, Type.TWO, 0, 0);
        assertEquals(cell.getState().getSymbol(Type.TWO), cell.getSymbol());

    }

    @Test
    public void When_StateIsOpenAndTypeIsThree_Expect_ToReturnCorrectSymbol() {
        Cell cell = new Cell(State.OPEN, Type.THREE, 0, 0);
        assertEquals(cell.getState().getSymbol(Type.THREE), cell.getSymbol());

    }

    @Test
    public void When_StateIsOpenAndTypeIsFour_Expect_ToReturnCorrectSymbol() {
        Cell cell = new Cell(State.OPEN, Type.FOUR, 0, 0);
        assertEquals(cell.getState().getSymbol(Type.FOUR), cell.getSymbol());

    }

    @Test
    public void When_StateIsOpenAndTypeIsFive_Expect_ToReturnCorrectSymbol() {
        Cell cell = new Cell(State.OPEN, Type.FIVE, 0, 0);
        assertEquals(cell.getState().getSymbol(Type.FIVE), cell.getSymbol());

    }

    @Test
    public void When_StateIsOpenAndTypeIsSix_Expect_ToReturnCorrectSymbol() {
        Cell cell = new Cell(State.OPEN, Type.SIX, 0, 0);
        assertEquals(cell.getState().getSymbol(Type.SIX), cell.getSymbol());

    }

    @Test
    public void When_StateIsOpenAndTypeIsSeven_Expect_ToReturnCorrectSymbol() {
        Cell cell = new Cell(State.OPEN, Type.SEVEN, 0, 0);
        assertEquals(cell.getState().getSymbol(Type.SEVEN), cell.getSymbol());

    }

    @Test
    public void When_StateIsOpenAndTypeIsEight_Expect_ToReturnCorrectSymbol() {
        Cell cell = new Cell(State.OPEN, Type.EIGHT, 0, 0);
        assertEquals(cell.getState().getSymbol(Type.EIGHT), cell.getSymbol());

    }

    @Test
    public void When_StateIsClosedAndTypeIsRandom_Expect_ToReturnCorrectSymbol() {
        Cell cell = new Cell(State.CLOSED, Type.EIGHT, 0, 0);
        assertEquals(cell.getState().getSymbol(Type.EIGHT), cell.getSymbol());

    }

    @Test
    public void When_StateIsFlagAndTypeIsRandom_Expect_ToReturnCorrectSymbol() {
        Cell cell = new Cell(State.FLAG, Type.EIGHT, 0, 0);
        assertEquals(cell.getState().getSymbol(Type.EIGHT), cell.getSymbol());

    }

    @Test
    public void When_StateIsQuestionAndTypeIsRandom_Expect_ToReturnCorrectSymbol() {
        Cell cell = new Cell(State.QUESTION, Type.EIGHT, 0, 0);
        assertEquals(cell.getState().getSymbol(Type.EIGHT), cell.getSymbol());

    }

    @Test
    public void When_OpenCellWhichIsClosed_Expect_ToReturnOpenState() {
        Cell cell = new Cell(State.QUESTION, Type.EIGHT, 0, 0);
        assertEquals(cell.getState().getSymbol(Type.EIGHT), cell.getSymbol());

    }

    @Test
    public void When_MarkClosedCell_Expect_ToReturnFlagState() {
        Cell cell = new Cell(State.CLOSED, Type.EIGHT, 0, 0);
        assertEquals(cell.mark(), State.FLAG);

    }

    @Test
    public void When_MarkClosedCell_Expect_CellStateToBecameFlag() {
        Cell cell = new Cell(State.CLOSED, Type.EIGHT, 0, 0);
        cell.mark();
        assertEquals(cell.getState(), State.FLAG);

    }

    @Test
    public void When_MarkOpenCell_Expect_ToReturnOpenState() {
        Cell cell = new Cell(State.OPEN, Type.EIGHT, 0, 0);
        assertEquals(cell.mark(), State.OPEN);

    }

    @Test
    public void When_MarkOpenCell_Expect_CellStateToRemainOpen() {
        Cell cell = new Cell(State.OPEN, Type.EIGHT, 0, 0);
        cell.mark();
        assertEquals(cell.getState(), State.OPEN);

    }

    @Test
    public void When_MarkFlagCell_Expect_ToReturnQuestionState() {
        Cell cell = new Cell(State.FLAG, Type.EIGHT, 0, 0);
        assertEquals(cell.mark(), State.QUESTION);

    }

    @Test
    public void When_MarkFlagCell_Expect_CellStateToBecameQuestion() {
        Cell cell = new Cell(State.FLAG, Type.EIGHT, 0, 0);
        cell.mark();
        assertEquals(cell.getState(), State.QUESTION);

    }

    @Test
    public void When_MarkQuestionCell_Expect_ToReturnClosedState() {
        Cell cell = new Cell(State.QUESTION, Type.EIGHT, 0, 0);
        assertEquals(cell.mark(), State.CLOSED);

    }

    @Test
    public void When_MarkQuestionCell_Expect_CellStateToBecameClosed() {
        Cell cell = new Cell(State.QUESTION, Type.EIGHT, 0, 0);
        cell.mark();
        assertEquals(cell.getState(), State.CLOSED);

    }

    @Test
    public void When_OpenClosedCell_Expect_CellStateToBecameOpen() {
        Cell cell = new Cell(State.OPEN, Type.EIGHT, 0, 0);
        cell.open();
        assertTrue(cell.getState() == State.OPEN);

    }

    @Test
    public void When_OpenFlagCell_Expect_CellStateRemainFlag() {
        Cell cell = new Cell(State.FLAG, Type.EIGHT, 0, 0);
        cell.open();
        assertTrue(cell.getState() == State.FLAG);

    }

    @Test
    public void When_OpenCellWhichIsAlreadyOpen_Expect_CellStateToRemainOpen() {
        Cell cell = new Cell(State.OPEN, Type.EIGHT, 0, 0);
        cell.open();
        assertTrue(cell.getState() == State.OPEN);

    }

    @Test
    public void When_OpenQuestionCell_Expect_CellStateToRemainQuestion() {
        Cell cell = new Cell(State.QUESTION, Type.EIGHT, 0, 0);
        cell.open();
        assertTrue(cell.getState() == State.QUESTION);

    }

}
