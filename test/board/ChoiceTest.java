package board;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ChoiceTest {

    Choice choice;

    @Test
    public void When_CellTypeIsCalled_Expect_ToReturnCorrectType() {
        choice = new Choice(new Cell(Type.MINE, 0, 0), 1);
        assertEquals(Type.MINE, choice.getCellType());
    }

    @Test
    public void When_CellWidthIsCalled_Expect_ToReturnCorrectWidth() {
        choice = new Choice(new Cell(Type.MINE, 0, 0), 1);
        assertEquals(0, choice.getWidth());
    }

    @Test
    public void When_CellHeighIsCalled_Expect_ToReturnCorrectHeigh() {
        choice = new Choice(new Cell(Type.MINE, 0, 0), 1);
        assertEquals(0, choice.getHeight());
    }

}
