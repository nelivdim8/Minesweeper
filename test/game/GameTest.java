package game;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import board.Board;
import board.Cell;
import board.Choice;
import board.Level;
import board.Type;
import input.Input;

@RunWith(MockitoJUnitRunner.class)
public class GameTest {
    @Mock
    public Input input;
    @Mock
    public Board board;
    @InjectMocks
    public Game game = new Game(Level.BEGINNER);

    @Test
    public void When_PlayerChooseCellAndOptionOne_Expect_ToReturnValidChoiceObject() {
        Cell cell = new Cell(Type.ONE, 2, 1);
        Choice choice = new Choice(cell, 1);
        when(input.getInput("Enter row: \n")).thenReturn(1);
        when(input.getInput("Enter column: \n")).thenReturn(2);
        when(board.getCell(2, 1)).thenReturn(cell);
        when(input.getInput("Choose option: \n1.Mark cell\n2.Open cell")).thenReturn(1);

        assertEquals(choice, game.getChoice());

    }

    @Test
    public void When_PlayerChooseCellAndOptionTwo_Expect_ToReturnValidChoiceObject() {
        Cell cell = new Cell(Type.ONE, 2, 1);
        Choice choice = new Choice(cell, 2);
        when(input.getInput("Enter row: \n")).thenReturn(1);
        when(input.getInput("Enter column: \n")).thenReturn(2);
        when(input.getInput("Choose option: \n1.Mark cell\n2.Open cell")).thenReturn(2);
        when(board.verifyCellIsNotMine(2, 1)).thenReturn(cell);
        assertEquals(choice, game.getChoice());

    }

    @Test
    public void When_ChoiceIs() {
        Cell cell = new Cell(Type.ONE, 2, 1);
        Choice choice = new Choice(cell, 1);
        when(input.getInput("Enter row: \n")).thenReturn(1);
        when(input.getInput("Enter column: \n")).thenReturn(2);
        when(board.getCell(2, 1)).thenReturn(cell);
        when(input.getInput("Choose option: \n1.Mark cell\n2.Open cell")).thenReturn(1);
        when(board.verifyCellIsNotMine(2, 1)).thenReturn(cell);
        assertEquals(choice, game.getChoice());

    }

    @Test
    public void When_CellIsEmpty_Expect_OpenEmptyCellsToBeCalled() {
        Choice choice = new Choice(new Cell(Type.EMPTY, 1, 2), 1);
        game.openCell(choice);
        verify(board, times(1)).openEmptyCells(1, 2);

    }

    @Test
    public void When_CellIsEmpty_Expect_OpenEmptyCellsToReturnTrue() {
        Choice choice = new Choice(new Cell(Type.EMPTY, 1, 2), 1);
        assertTrue(game.openCell(choice));

    }

    @Test
    public void When_CellContainsNumber_Expect_OpenEmptyCellsToReturnTrue() {
        Choice choice = new Choice(new Cell(Type.ONE, 1, 2), 1);
        assertTrue(game.openCell(choice));

    }

    @Test
    public void When_CellContainsNumber_Expect_OpenCellToBeCalled() {
        Choice choice = new Choice(new Cell(Type.ONE, 1, 2), 1);
        game.openCell(choice);
        verify(board, times(1)).openCell(1, 2);

    }

    @Test
    public void When_CellContainsMine_Expect_OpenEmptyCellsToReturnFalse() {
        Choice choice = new Choice(new Cell(Type.MINE, 1, 2), 1);
        assertFalse(game.openCell(choice));

    }

    @Test
    public void When_CellContainsNumber_Expect_OpenMinesToBeCalled() {
        Choice choice = new Choice(new Cell(Type.MINE, 1, 2), 1);
        game.openCell(choice);
        verify(board, times(1)).openMines();

    }

}
